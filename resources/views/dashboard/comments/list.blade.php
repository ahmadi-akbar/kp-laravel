@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h4>Comments</h4>
          </div>
          <div class="card-body">
            <table class="table table-responsive-sm table-striped">
              <thead>
                <tr>
                  <th class="text-center">Comment</th>
                  <th class="text-center">User</th>
                  <th class="text-center">Artist</th>
                  <th class="text-center">Music</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $i)
                <tr>
                  <td class="text-center">
                    {{ Str::words($i->text, 10, '...') }}
                  </td>
                  <td class="text-center">{{ $i->user->email }}</td>
                  <td class="text-center">{{ Str::limit($i->music->artist->name, 15, '...') }}</td>
                  <td class="text-center">{{ Str::limit($i->music->title, 15, '...') }}</td>
                  <td>
                    <div class="d-flex align-items-center justify-content-around">
                      <form action="{{ route('comments.destroy', $i->id ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-outline-danger" data-toggle="tooltip" title="Delete"><i class="cil-trash"></i></button>
                      </form>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@section('javascript')

@endsection