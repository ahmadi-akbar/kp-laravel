<footer class="c-footer">
  <div><a href="https://karaokepersia.com">Karaoke Persia</a> &copy; 2020</div>
  <div class="ml-auto">Powered by&nbsp;<a href="https://www.nikan.ir">Nikan</a></div>
</footer>