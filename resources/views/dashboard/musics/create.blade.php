@extends('dashboard.base')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.min.css')}}">
@endsection


@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-10 col-lg-12 col-xl-12">
        @if(Session::has('message'))
        <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4>Add Music</h4>
          </div>
          <div class="card-body">

            <form method="POST" action="{{ route('musics.store') }}" id="main">
              @csrf

              <div class="form-group row">
                <div class="col-4">
                  <label>Title</label>
                  <input class="form-control" type="text" placeholder="Title" name="title" value="{{ old('title') }}" required autofocus>
                </div>
                <div class="col-4">
                  <label>Artist</label>
                  <select class="form-control" name="artist_id" value="{{ old('artist_id') }}">
                    @foreach($artists as $i)
                    <option value="{{ $i->id }}">{{ $i->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-4">
                  <label>Price</label>
                  <input class="form-control" type="number" min="0" name="price" required value="{{ old('price') }}">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-6">
                  <label>Original Song</label>
                  <div class="dropzone needsclick" id="url"></div>
                </div>
                <div class="col-6">
                  <label>Cover</label>
                  <div class="dropzone needsclick" id="cover"></div>
                </div>

              </div>

              <div class="form-group row">
                <div class="col-6">
                  <label>Karaoke Song</label>
                  <div class="dropzone needsclick" id="k-url"></div>
                </div>
                <div class="col-6">
                  <label>Cover</label>
                  <div class="dropzone needsclick" id="k-cover"></div>
                </div>
              </div>


              <div class="d-flex justify-content-around">
                <a href="{{ route('musics.index') }}" class="btn btn-primary col-3">Return</a>
                <button class="btn btn-success col-3" type="submit">Add</button>
              </div>

              <input type="hidden" name="cover" value="{{ old('cover') }}">
              <input type="hidden" name="kCover" value="{{ old('kCover') }}">
              <input type="hidden" name="url" value="{{ old('url') }}">
              <input type="hidden" name="kUrl" value="{{ old('kUrl') }}">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')

<script src="{{asset('js/dropzone.min.js')}}" type="text/javascript"></script>
<script>
  // Dropzone.autoDiscover = false;
  function editForm(name, val) {
    var inp = document.querySelector('form#main > input[name="' + name + '"]')
    if (inp)
      inp.setAttribute("value", val);
  }

  var configs = {
    url: "{{ route('upload') }}",
    maxFilesize: 1,
    acceptedFiles: "image/*",
    addRemoveLinks: false,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    maxFiles: 1,
    init: function() {
      this.on("maxfilesexceeded", function(file) {
        this.removeAllFiles();
        this.addFile(file);
      });
    },
    error: function(file, response) {
      console.log('err =>', response);
      return false;
    }
  }

  Dropzone.options.url = {
    ...configs,
    maxFilesize: 50,
    acceptedFiles: "video/*",
    success: function(file, res) {
      editForm('url', res.success);
    },
  }
  Dropzone.options.kUrl = {
    ...configs,
    maxFilesize: 50,
    acceptedFiles: "video/*",
    success: function(file, res) {
      editForm('kUrl', res.success);
    },
  }

  Dropzone.options.cover = {
    ...configs,
    success: function(file, res) {
      editForm('cover', res.success);
    },
  }

  Dropzone.options.kCover = {
    ...configs,
    success: function(file, res) {
      editForm('kCover', res.success);
    },
  }
</script>
@endsection