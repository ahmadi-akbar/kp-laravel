@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h4>Music</h4>
            <a href="{{ route('musics.create') }}" class="btn btn-primary">Add</a>
          </div>
          <div class="card-body">
            <table class="table table-responsive-sm table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Artist</th>
                  <th class="text-center">Cover</th>
                  <th class="text-center">Karaoke Cover</th>
                  <th class="text-center">Premium</th>
                  <th class="text-center">Price</th>
                  <th class="text-center"># Likes</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $i)
                <tr>
                  <td>
                    <strong>{{ $i->title }}</strong>
                  </td>
                  <td>{{ $i->artist->name }}</td>
                  <td class="text-center" style="height: 85px;">
                    <img src={{ asset("public/"."{$i->cover}") }} height="60" alt="Cover" />
                  </td>
                  <td class="text-center" style="height: 85px;">
                    <img src={{ asset("public/"."{$i->kCover}") }} height="60" alt="Cover" />
                  </td>
                  <td class="text-center">
                    @if($i->price)
                    <i class="text-success h3 cil-check-circle"></i>
                    @endif
                  </td>
                  <td class="text-center">
                    @if($i->price)
                    {{ $i->price }}
                    @else
                    <strong>free</strong>
                    @endif
                  </td>
                  <td class="text-center">{{ $i->like_count }}</td>
                  <td>
                    <div class="d-flex align-items-center justify-content-around">
                      <!-- <a href="{{ url('/musics/' . $i->id) }}" class="btn btn-outline-primary" data-toggle="tooltip" title="View"><i class="cil-magnifying-glass"></i></a> -->
                      <a href="{{ url('/musics/' . $i->id . '/edit') }}" class="btn btn-outline-warning" data-toggle="tooltip" title="Edit"><i class="cil-pencil"></i></a>
                      <form action="{{ route('musics.destroy', $i->id ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-outline-danger" data-toggle="tooltip" title="Delete"><i class="cil-trash"></i></button>
                      </form>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            {{ $items->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@section('javascript')

@endsection