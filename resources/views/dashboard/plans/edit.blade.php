@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-10 col-lg-8 col-xl-8">
        <div class="card">
          <div class="card-header">
            <i class="fa fa-align-justify"></i>Edit Plan: {{ $item->id }}
          </div>
          <div class="card-body">
            <form method="POST" action="{{ url('plans/'.$item->id) }}">
              @csrf
              @method('PUT')
              <div class="form-group row">
                <div class="col-6">
                  <label>Price</label>
                  <input class="form-control" type="number" min="0" name="price" required autofocus value="{{ $item->price }}">
                </div>
                <div class="col-6">
                  <label># Coins</label>
                  <input class="form-control" type="number" min="0" name="coin" required value="{{ $item->coin }}">
                </div>
              </div>

              <div class="d-flex justify-content-around">
                <a href="{{ route('plans.index') }}" class="btn btn-primary w-25">Return</a>
                <button class="btn btn-success w-25" type="submit">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')

@endsection