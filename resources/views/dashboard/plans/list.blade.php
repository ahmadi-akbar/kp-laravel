@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h4>Plans</h4>
            <a href="{{ route('plans.create') }}" class="btn btn-primary">Add</a>
          </div>
          <div class="card-body">
            <table class="table table-responsive-sm table-striped">
              <thead>
                <tr>
                  <th class="text-center">Price</th>
                  <th class="text-center"># Coins</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $i)
                <tr>
                  <td class="text-center">
                    <strong>${{ $i->price }}</strong>
                  </td>
                  <td class="text-center">{{ $i->coin }}</td>
                  <td>
                    <!-- <div class="d-flex align-items-center justify-content-around">
                      <a href="{{ url('/plans/' . $i->id . '/edit') }}" class="btn btn-outline-warning" data-toggle="tooltip" title="Edit"><i class="cil-pencil"></i></a>
                      <form action="{{ route('plans.destroy', $i->id ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-outline-danger" data-toggle="tooltip" title="Delete"><i class="cil-trash"></i></button>
                      </form>
                    </div> -->
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@section('javascript')

@endsection