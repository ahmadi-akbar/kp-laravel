@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-10 col-lg-8 col-xl-8">
        @if(Session::has('message'))
        <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <div class="card-header">
              <h4>Add Plan</h4>
            </div>
            <div class="card-body">

              <form method="POST" action="{{ route('plans.store') }}" id="main">
                @csrf

                <div class="form-group row">
                  <div class="col-6">
                    <label>Price</label>
                    <input class="form-control" type="number" min="0" name="price" required autofocus value="0">
                  </div>
                  <div class="col-6">
                    <label># Coins</label>
                    <input class="form-control" type="number" min="0" name="coin" required value="0">
                  </div>
                </div>

                <div class="d-flex justify-content-around">
                  <a href="{{ route('plans.index') }}" class="btn btn-primary col-3">Return</a>
                  <button class="btn btn-success col-3" type="submit">Add</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @endsection