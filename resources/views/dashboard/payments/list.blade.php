@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="col-sm-12 col-md-10 col-lg-12 col-xl-12">
          @if(Session::has('message'))
          <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          <div class="card">
            <div class="card-header">
              <h4 style="text-transform: capitalize;">{{$status}} Payments</h4>
            </div>
            <div class="card-body">
              <table class="table table-responsive-sm table-striped">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Voucher</th>
                    <th>Code</th>
                    <th class="text-center">Price</th>
                    <th>Request Date</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($items as $item)
                  <tr>
                    <td>
                      <strong>{{ $item->user->name }}</strong>
                    </td>
                    <td>{{ $item->voucher }}</td>
                    <td>{{ $item->code }}</td>
                    <td class="text-center">
                      <strong>${{ $item->plan->price }}</strong>
                    </td>
                    <td>{{ $item->created_at->format('y-m-d H:i') }}</td>
                    <td class="text-center">
                      @if($item->status == 'valid')
                      <i class="text-success h3 cil-check-circle" data-toggle="tooltip" title="Accepted"></i>
                      @elseif($item->status == 'reject')
                      <i class="text-danger h3 cil-x-circle" data-toggle="tooltip" title="Rejected"></i>
                      @else
                      <span class="text-warning">pending</span>
                      @endif
                    </td>
                    <td>
                      <div class="d-flex align-items-center justify-content-around">
                        <!-- <a href="{{ url('/payment/' . $item->id) }}" class="btn btn-outline-primary" data-toggle="tooltip" title="View"><i class="cil-magnifying-glass"></i></a> -->
                        @if($item->status == 'valid')
                        <span class="text-success">{{$item->updated_at->format('Y-m-d')}}</span>
                        @elseif($item->status == 'reject')
                        <span class="text-danger">{{$item->updated_at->format('Y-m-d')}}</span>
                        @else
                        <form action="{{ route('payments.update', $item->id ) }}" method="POST">
                          @method('put')
                          @csrf
                          <input name="status" hidden value="valid" />
                          <button class="btn btn-outline-success" data-toggle="tooltip" title="Accept"><i class="cil-check-circle"></i></button>
                        </form>
                        <form action="{{ route('payments.update', $item->id ) }}" method="POST">
                          @method('put')
                          @csrf
                          <input name="status" hidden value="reject" />
                          <button class="btn btn-outline-danger" data-toggle="tooltip" title="Reject"><i class="cil-x-circle"></i></button>
                        </form>
                        @endif

                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $items->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @endsection