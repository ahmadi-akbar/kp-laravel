@extends('dashboard.base')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.min.css')}}">
@endsection

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-10 col-lg-8 col-xl-8">
        <div class="card">
          <div class="card-header">
            <h4>Add Artist</h4>
          </div>
          <div class="card-body">
            <form method="POST" action="{{ route('artists.store') }}" id="main">
              @csrf
              <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>
              </div>

              <div class="form-group">
                <label>Cover</label>
                <div class="dropzone needsclick" id="cover"></div>
              </div>

              <div class="d-flex justify-content-around">
                <a href="{{ route('artists.index') }}" class="btn btn-primary col-4">Return</a>
                <button class="btn btn-success col-4" type="submit">Add</button>
              </div>

              <input type="hidden" name="cover" value="{{ old('cover') }}">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')

<script src="{{asset('js/dropzone.min.js')}}" type="text/javascript"></script>
<script>
  // Dropzone.autoDiscover = false;
  function editForm(name, val) {
    var inp = document.querySelector('form#main > input[name="' + name + '"]')
    if (inp)
      inp.setAttribute("value", val);
  }

  var configs = {
    url: "{{ route('upload') }}",
    maxFilesize: 1,
    acceptedFiles: "image/*",
    addRemoveLinks: false,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    maxFiles: 1,
    init: function() {
      this.on("maxfilesexceeded", function(file) {
        this.removeAllFiles();
        this.addFile(file);
      });
    },
    error: function(file, response) {
      console.log('err =>', response);
      return false;
    }
  }

  Dropzone.options.cover = {
    ...configs,
    success: function(file, res) {
      editForm('cover', res.success);
    },
  }
</script>
@endsection