@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h4>Artists</h4>
            <a href="{{ route('artists.create') }}" class="btn btn-primary">Add</a>
          </div>
          <div class="card-body">
            <table class="table table-responsive-sm table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th class="text-center">Cover</th>
                  <th class="text-center"># Musics</th>
                  <th class="text-center"># Follow</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $i)
                <tr>
                  <td>
                    <strong>{{ $i->name }}</strong>
                  </td>
                  <td class="text-center" style="height: 85px;">
                    <img src={{ asset("public/"."{$i->cover}") }} height="60" alt="Cover" />
                  </td>
                  <td class="text-center">{{ $i->music_count }}</td>
                  <td class="text-center">{{ $i->follow_count }}</td>
                  <td>
                    <div class="d-flex align-items-center justify-content-around">
                      <a href="{{ url('/artists/' . $i->id . '/edit') }}" class="btn btn-outline-warning" data-toggle="tooltip" title="Edit"><i class="cil-pencil"></i></a>
                      <form action="{{ route('artists.destroy', $i->id ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-outline-danger" data-toggle="tooltip" title="Delete"><i class="cil-trash"></i></button>
                      </form>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            {{ $items->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection