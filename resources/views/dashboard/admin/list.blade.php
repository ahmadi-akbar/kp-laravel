@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h4>Users</h4>
            <div class="d-flex align-items-center">
              #Trial Coins:
              <span class="font-weight-bold">&nbsp; {{ $trial_coins }}</span>
              <a href="{{ url('/user/configs') }}" class="btn btn-outline-warning ml-2" data-toggle="tooltip" title="Edit"><i class="cil-pencil"></i></a>
            </div>
          </div>
          <div class="card-body">
            <table class="table table-responsive-sm table-striped">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>E-mail</th>
                  <th class="text-center">Coins</th>
                  <th class="text-center">Joined at</th>
                  <th class="text-center">Verified</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td>
                    <strong>{{ $user->name }}</strong>
                  </td>
                  <td>{{ $user->email }}</td>
                  <td class="text-center">{{ $user->coins }}</td>
                  <td class="text-center">{{ $user->created_at->format('y-m-d H:i') }}</td>
                  <td class="text-center">
                    @if($user->email_verified_at)
                    <i class="text-success h3 cil-check-circle"></i>
                    @else
                    <i class="text-danger h3 cil-x-circle"></i>
                    @endif
                  </td>
                  <td>
                    <div class="d-flex align-items-center justify-content-around">
                      <a href="{{ url('/users/' . $user->id) }}" class="btn btn-outline-primary" data-toggle="tooltip" title="View"><i class="cil-magnifying-glass"></i></a>
                      <a href="{{ url('/users/' . $user->id . '/edit') }}" class="btn btn-outline-warning" data-toggle="tooltip" title="Edit"><i class="cil-pencil"></i></a>

                      <!-- <form action="{{ route('users.destroy', $user->id ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-outline-danger" data-toggle="tooltip" title="Delete"><i class="cil-trash"></i></button>
                      </form> -->

                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            {{ $users->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection