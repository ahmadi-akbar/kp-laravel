@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row justify-content-center">
      <div class="col-sm-6 col-md-5 col-lg-6 col-xl-6">
        <div class="card">
          <div class="card-header">
            <h4>Edit {{ $user->name }}</h4>
          </div>
          <div class="card-body">
            <form method="POST" action="{{ url('users/'.$user->id) }}">
              @csrf
              @method('PUT')
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <svg class="c-icon c-icon-sm">
                      <use xlink:href="/assets/icons/coreui/free-symbol-defs.svg#cui-user"></use>
                    </svg>
                  </span>
                </div>
                <input class="form-control" type="text" placeholder="Name" name="name" value="{{ $user->name }}" required autofocus>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">@</span>
                </div>
                <input class="form-control" type="text" placeholder="E-Mail Address" name="email" value="{{ $user->email }}" required readonly>
              </div>

              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"># Coins</span>
                </div>
                <input class="form-control" type="number" placeholder="# of coins" min="0" name="coins" value="{{ $user->coins }}" required>
              </div>

              <button class="btn btn-block btn-success" type="submit">Save</button>
              <a href="{{ route('users.index') }}" class="btn btn-block btn-primary">Return</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection