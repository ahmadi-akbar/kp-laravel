@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row justify-content-center">
      <div class="col-sm-6 col-md-5 col-lg-6 col-xl-6">
        <div class="card">
          <div class="card-header">
            <h4>Edit Number of Trial Coins</h4>
          </div>
          <div class="card-body">
            <form method="POST" action={{url('/user/configs')}}>
              @csrf
              <div class="form-group mb-3">
                <label class="h6"># of coins</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">#</span>
                  </div>
                  <input class="form-control" type="number" placeholder="# of coins" min="0" name="coins" value="{{ $trial_coins }}" required autofocus>
                </div>
              </div>
              <button class="btn btn-block btn-success" type="submit">Save</button>
              <a href="{{ route('users.index') }}" class="btn btn-block btn-primary">Return</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection