@extends('dashboard.base')

@section('content')

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row justify-content-center">
      <div class="col-sm-6 col-md-5 col-lg-8 col-xl-8">
        <div class="card">
          <div class="card-header">
            <h4>User {{ $user->name }}</h4>
          </div>
          <div class="card-body">
            <div class="d-flex justify-content-around align-items-center mb-5">

              <img src={{ asset("public/$user->avatar") }} height="200" width="200" style="border-radius:50%;" alt="Avatar" />
              <div>
                <h4>
                  Name: <span class="font-weight-normal">{{ $user->name }}</span>
                </h4>
                <h4>
                  Coins: <span class="font-weight-normal">{{ $user->coins }}</span>
                </h4>
                <h4>
                  Email: <span class="font-weight-normal">{{ $user->email }}</span>
                </h4>
                <h4>
                  Joined at: <span class="font-weight-normal">{{ $user->created_at->format('Y-m-d H:i') }}</span>
                </h4>
              </div>

            </div>
            <a href="{{ route('users.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection