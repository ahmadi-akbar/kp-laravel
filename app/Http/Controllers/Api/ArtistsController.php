<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SuccessResource;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\ArtistWithMusicResource;
use App\Http\Resources\MusicResource;
use App\Models\Artists;
use App\Models\Follows;
use App\Models\Musics;
use Illuminate\Support\Facades\Auth;

class ArtistsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Artists::withCount('music')->paginate(20);
        return ArtistWithMusicResource::collection($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function follow($id)
    {
        $user_id = Auth::id();
        $item = Artists::find($id);
        if (!$item) {
            $res         = new \stdClass();
            $res->errors = "Not Found !!";
            return (new ErrorResource($res))
                ->response()
                ->setStatusCode(404);
        }

        $i = Follows::firstOrCreate(['artist_id'  => $id, 'user_id' => $user_id]);
        if ($i->wasRecentlyCreated) {
            $item->increment('follow_count');
            $item->save();
        }
        return new SuccessResource('followed');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function unFollow($id)
    {
        $user_id = Auth::id();
        $item = Artists::find($id);
        if (!$item) {
            $res         = new \stdClass();
            $res->errors = "Not Found !!";
            return (new ErrorResource($res))
                ->response()
                ->setStatusCode(404);
        }

        $i = Follows::firstWhere(['artist_id'  => $id, 'user_id' => $user_id]);
        if ($i) {
            Follows::destroy($i->id);
            $item->decrement('follow_count');
            $item->save();
        }
        return new SuccessResource('UnFollowed !!');
    }

    public function getMusics($id)
    {
        $items = Musics::where('artist_id', $id)->paginate(20);
        return MusicResource::collection($items);
    }
}
