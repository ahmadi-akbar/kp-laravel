<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Http\Resources\SuccessResource;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\MusicResource;
use App\Http\Resources\SearchResource;
use App\Models\Artists;
use App\Models\Comments;
use App\Models\Musics;
use App\Models\Likes;
use App\Models\Purchases;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MusicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {

        $cat = $req->cat;
        $items = Musics::with(['artist', 'like']);

        if ($cat == 'popular')
            $items = $items->orderBy('like_count', 'desc');
        else if ($cat == 'recent')
            $items = $items->orderBy('created_at', 'desc');
        else if ($cat == 'premium')
            $items = $items->orderBy('like_count', 'desc')->where('price', '!=', 0);
        else if ($cat == 'free')
            $items = $items->orderBy('like_count', 'desc')->where('price', 0);


        return MusicResource::collection($items->paginate(20));
    }


    public function like($id)
    {
        $user_id = Auth::id();
        $item = Musics::find($id);
        if (!$item) {
            $res         = new \stdClass();
            $res->errors = "Not Found !!";
            return (new ErrorResource($res))
                ->response()
                ->setStatusCode(404);
        }

        $i = Likes::firstOrCreate(['music_id'  => $id, 'user_id' => $user_id]);

        if ($i->wasRecentlyCreated) {
            $item->increment('like_count');
            $item->save();
        }

        return new SuccessResource('liked !!');
    }


    public function unLike($id)
    {
        $user_id = Auth::id();
        $item = Musics::find($id);
        if (!$item) {
            $res         = new \stdClass();
            $res->errors = "Not Found !!";
            return (new ErrorResource($res))
                ->response()
                ->setStatusCode(404);
        }

        $i = Likes::firstWhere(['music_id'  => $id, 'user_id' => $user_id]);
        if (!$i) {
            return new SuccessResource('liked not found !!');
        }

        Likes::destroy($i->id);
        $item->decrement('like_count');
        $item->save();
        return new SuccessResource('UnLiked !!');
    }

    public function search(Request $req)
    {
        $q = $req->q;
        $music = Musics::where('title', 'like', "%{$q}%")->paginate(20);
        $artist = Artists::where('name', 'like', "%{$q}%")->paginate(20);

        return new SearchResource(['music' => $music, 'artist' => $artist]);
    }

    public function getComments($id)
    {
        $items = Comments::with('user')->where('music_id', $id)->orderBy('created_at')->paginate(20);
        return CommentResource::collection($items);
    }

    public function addComment(Request $req, $id)
    {
        $req->validate([
            'text' => 'required|string|min:1|max:10000'
        ]);

        $user_id = Auth::id();
        Comments::create(['music_id'  => $id, 'user_id' => $user_id, 'text' => $req->text]);

        return new SuccessResource('Created !!');
    }

    public function purchaseMusic($id)
    {
        $m = Musics::find($id);
        $user = Auth::user();
        $price = $m->price;

        if ($user->coins < $price) {
            return (new ErrorResource('You have not Enough coins.'))
                ->response()
                ->setStatusCode(404);
        }

        $a = Purchases::firstOrCreate(['user_id' => $user->id, 'music_id' => $m->id]);
        if ($a->wasRecentlyCreated) {
            $user->decrement('coins', $price);
            $user->save();
        }

        return new SuccessResource('Purchased Successfully !!');
    }
}
