<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\SuccessResource;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Mail;


class AuthController extends Controller
{

    function generatePass($length)
    {
        $chars = "0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    public function login(Request $req)
    {

        $v = Validator::make($req->all(), [
            "email" => "email|required",
            "password" => "required"
        ]);
        if ($v->fails()) {
            return (new ErrorResource('Error in Login', $v->errors()))
                ->response()
                ->setStatusCode(403);
        }

        $user = User::where("email", $req->email)->first();
        if (!$user) {
            return (new ErrorResource('You are not REGISTERED!!', []))
                ->response()
                ->setStatusCode(403);
        }

        $credentials = $req->only(["email", "password"]);
        if (!Auth::attempt($credentials)) {
            return (new ErrorResource('Invalid Credential', []))
                ->response()
                ->setStatusCode(500);
        }

        $tokenResult = $user->createToken("authToken")->plainTextToken;
        return new SuccessResource('LoggedIn SuccessFully', [
            "access_token" => $tokenResult,
            "token_type" => "Bearer",
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $req)
    {
        $v = Validator::make($req->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($v->fails()) {
            return (new ErrorResource('Error in Register', $v->errors()))
                ->response()
                ->setStatusCode(403);
        }

        $trial_coins = User::find(1)->coins;
        $user = User::create([
            'name' => $req['name'],
            'email' => $req['email'],
            'password' => bcrypt($req['password']),
            'coins'     => $trial_coins,
        ]);
        $tokenResult = $user->createToken("authToken")->plainTextToken;
        return new SuccessResource('Registered Successfully', [
            "access_token" => $tokenResult,
            "token_type" => "Bearer",
        ]);
    }

    protected function changePass(Request $req)
    {
        $v = Validator::make($req->all(), [
            'password' => 'required|string|min:6',
        ]);
        if ($v->fails()) {
            return (new ErrorResource('Error in Change Pass', $v->errors()))
                ->response()
                ->setStatusCode(403);
        }

        User::where('id', Auth::id())->update(['password' => bcrypt($req->password)]);
        return new SuccessResource('Password changed successfully.',);
    }

    protected function forgetPass(Request $req)
    {
        $v = Validator::make($req->all(), [
            'email' => 'required|string|email|max:255',
        ]);

        if ($v->fails()) {
            return (new ErrorResource('Error in forget Pass', $v->errors()))
                ->response()
                ->setStatusCode(403);
        }

        $user = User::where("email", $req->email)->first();
        if (!$user) {
            return (new ErrorResource('You are not REGISTERED!!', []))
                ->response()
                ->setStatusCode(403);
        }

        $newPass = $this->generatePass(8);

        $to = $user->email;
        $mail = 'Your new password is ' . $newPass . '.';
        Mail::send([], [], function ($message) use ($to, $mail) {
            $message->to($to);
            $message->subject('Reset Password');
            $message->setBody($mail, 'text/html');
        });
        $user->update(['password' => bcrypt($newPass)]);
        return new SuccessResource('New password sent to your mail address.');
    }
}
