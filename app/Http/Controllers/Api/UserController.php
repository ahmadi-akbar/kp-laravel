<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\FollowResource;
use App\Http\Resources\LikeResource;
use App\Http\Resources\PaymentResource;
use App\Http\Resources\PurchasedMusicResource;
use App\Http\Resources\SuccessResource;
use App\Models\User;
use App\Models\Follows;
use App\Models\Likes;
use App\Models\Payments;
use App\Models\Purchases;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $item = User::find($id);
        return new UserResource($item);
    }


    public function getFollowList()
    {
        $items = Follows::with('artist')->where('user_id', Auth::id())->paginate(20);
        return FollowResource::collection($items);
    }

    public function getLikeList()
    {
        $items = Likes::with('music')->where('user_id', Auth::id())->paginate(20);
        return LikeResource::collection($items);
    }


    public function getPurchasedMusicList()
    {
        $items = Purchases::with('music')->where('user_id', Auth::id())->paginate(20);

        return PurchasedMusicResource::collection($items);
    }

    public function getPayments()
    {
        $items = Payments::orderBy('created_at', 'desc')->with('plan')->where('user_id', Auth::id())->paginate(20);

        return PaymentResource::collection($items);
    }


    public function saveFcmToken(Request $req)
    {
        $req->validate([
            'fcm_token' => 'required|string|min:100|max:255'
        ]);

        $user = User::find(Auth::id());
        $user->fcm_token = $req->fcm_token;
        $user->save();

        return new SuccessResource('Token Saved');
    }

    public function update(Request $req)
    {
        $req->validate([
            'name' => 'required|string|max:255',
        ]);
        $avatar = NULL;
        $f = $req->file('avatar');
        if ($f)
            $avatar = Storage::disk('public')->put('avatar', $f);

        $user = User::find(Auth::id());
        $user->update(['name' => $req->name, 'avatar' => $avatar]);
        $newUser = $user->fresh();

        return new SuccessResource('Saved Successfully', $newUser);
    }
}
