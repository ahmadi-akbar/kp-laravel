<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PlanResource;
use App\Models\Payments;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\SuccessResource;
use App\Models\Plans;

class PlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plans::orderBy('updated_at', 'desc')->get();
        return PlanResource::collection($plans);
    }

    public function purchase(Request $req, $id)
    {
        $user_id = Auth::id();
        $req->validate([
            'code'             => 'required|min:1|max:64',
            'voucher'           => 'required',
        ]);

        $item = new Payments();
        $item->plan_id     = $id;
        $item->user_id     = $user_id;
        $item->code        = $req->code;
        $item->voucher     = $req->voucher;
        $item->save();
        return new SuccessResource('Thanks for your purchase 👍');
    }
}
