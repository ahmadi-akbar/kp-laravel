<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trial_coins = User::find(1)->coins;
        $users = User::where('role', 'user')->orderBy('created_at', 'desc')->paginate(5);
        return view('dashboard.admin.list', compact('users', 'trial_coins'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('dashboard.admin.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.admin.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $v = $req->validate([
            'name'       => 'required|min:1|max:256',
            'email'      => 'required|email|max:256',
            'coins'      => 'required|min:1',
        ]);
        $user = User::find($id);
        $user->name       = $req->input('name');
        $user->email      = $req->input('email');
        $user->coins      = $req->input('coins');
        $user->save();
        $req->session()->flash('message', 'Successfully updated user');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
        }
        return redirect()->route('users.index');
    }

    public function configView()
    {
        $trial_coins = User::find(1)->coins;
        return view('dashboard.admin.config', compact('trial_coins'));
    }
    public function configStore(Request $req)
    {
        $v = $req->validate([
            'coins'       => 'required|min:1',
        ]);
        $user = User::find(1);
        $user->coins       = $req->input('coins');
        $user->save();
        $req->session()->flash('message', 'Successfully updated user');
        return redirect()->route('users.index');
    }
}
