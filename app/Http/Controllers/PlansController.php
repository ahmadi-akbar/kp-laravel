<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Plans;

class PlansController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Plans::orderBy('created_at', 'desc')->paginate(20);
        return view('dashboard.plans.list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {

        $v = Validator::make($req->all(), [
            'price'           => 'required',
            'coin'           => 'required',
        ], [
            'cover.required' => 'The Original Song Cover field is required.',
            'kCover.required' => 'The Karaoke Cover field is required.',
            'url.required' => 'The Song field is required.',
            'kUrl.required' => 'The Karaoke field is required.',
        ])->validate();


        $fields = $req->all(['price', 'coin']);
        Plans::create($fields);

        $req->session()->flash('message', 'Successfully created note');
        return redirect()->route('plans.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Plans::find($id);
        return view('dashboard.plans.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $v = $req->validate([
            'price'             => 'required',
            'coin'           => 'required',
        ]);

        $item = Plans::find($id);
        $item->price     = $req->input('price');
        $item->coin   = $req->input('coin');
        $item->save();
        $req->session()->flash('message', 'Successfully edited note');
        return redirect()->route('plans.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Plans::find($id);
        if ($item) {
            $item->delete();
        }
        return redirect()->route('plans.index');
    }
}
