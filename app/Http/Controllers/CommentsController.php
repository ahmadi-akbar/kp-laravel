<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comments;

class CommentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $items = Comments::orderBy('created_at', 'desc')->with(['user', 'music']);

        $items = $items->paginate(20);

        return view('dashboard.comments.list', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Comments::find($id);
        if ($item) {
            $item->delete();
        }
        return redirect()->route('comments.index');
    }
}
