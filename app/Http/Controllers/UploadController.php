<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    function index(Request $req)
    {
        $f = $req->file('file');
        // $name = time() . $f->getClientOriginalName();
        $res = Storage::disk('public')->put('media', $f);
        return response()->json(['success' => $res]);
    }

    function delete(Request $req)
    {
        if ($req->get('name')) {
            File::delete(public_path('images/' . $req->get('name')));
        }
    }
}
