<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artists;

class ArtistsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Artists::withCount('music')->orderBy('created_at', 'desc')->paginate(20);
        return view('dashboard.artists.list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.artists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $v = $req->validate([
            'name'             => 'required|min:1|max:64',
            'cover'           => 'required',
        ]);

        $item = new Artists();
        $item->name     = $req->input('name');
        $item->cover   = $req->input('cover');
        $item->save();
        $req->session()->flash('message', 'Successfully created');
        return redirect()->route('artists.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Artists::find($id);
        return view('dashboard.artists.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $v = $req->validate([
            'name'             => 'required|min:1|max:64',
            'cover'           => 'required',
        ]);

        $item = Artists::find($id);
        $item->name     = $req->input('name');
        $item->cover   = $req->input('cover');
        $item->save();
        $req->session()->flash('message', 'Edited Successfully');
        return redirect()->route('artists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Artists::find($id);
        if ($item) {
            $item->delete();
        }
        return redirect()->route('artists.index');
    }
}
