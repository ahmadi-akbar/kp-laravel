<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payments;

class PaymentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $status = $req->query('status', NULL);
        $items = Payments::orderBy('created_at', 'desc')->with(['user', 'plan']);
        if ($status) {
            $items = $items->where('status', $status);
        }

        $items = $items->paginate(20);

        return view('dashboard.payments.list', compact('items', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $item             = Payments::where('id', $id)->with(['user', 'plan'])->first();
        $item->status     = $req->status;
        if ($req->status === 'valid') {
            $item->user->increment('coins', $item->plan->coin);
        }
        $item->save();
        $req->session()->flash('message', 'Updated Successfully!');
        return redirect()->route('payments.index');
    }
}
