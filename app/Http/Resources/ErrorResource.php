<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ErrorResource extends JsonResource
{

    public static $wrap = 'errors';

    public function __construct($message, $data = [])
    {
        $this->message = $message;
        $this->data = $data;
    }

    public function toArray($request)
    {
        return $this->data;
    }

    public function with($request)
    {
        return [
            'message' => $this->message,
            'success' => false,
        ];
    }
}
