<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class ArtistWithMusicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            => $this->id,
            "name"          => $this->name,
            "cover"         => $this->cover,
            "follow_count"  => $this->follow_count,
            "created_at"    => $this->created_at,
            "music_count"   => $this->music_count,
        ];
    }
}
