<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LikeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            => $this->music->id,
            "title"         => $this->music->title,
            "cover"         => $this->music->cover,
            "price"         => $this->music->price,
            "artist"        => new ArtistResource($this->music->artist),
        ];
    }
}
