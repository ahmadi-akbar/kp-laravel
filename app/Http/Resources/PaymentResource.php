<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                => $this->id,
            "created_at"        => $this->created_at->format('Y-m-d H:i'),
            "coins"             => $this->plan->coin,
            "price"             => $this->plan->price,
            "status"            => $this->status,
        ];
    }
}
