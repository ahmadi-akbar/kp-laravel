<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MusicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            => $this->id,
            "title"         => $this->title,
            "url"           => $this->url,
            "kUrl"          => $this->kUrl,
            "cover"         => $this->cover,
            "kCover"        => $this->kCover,
            "price"         => $this->price,
            "artist"        => [
                "id"        => $this->artist->id,
                "name"      => $this->artist->name,
            ],
            "like"          => $this->isLiked(),
            "purchase"      => empty($this->price) ?: $this->isPurchased()
        ];
    }
}
