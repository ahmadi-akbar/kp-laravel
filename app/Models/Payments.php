<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Payments extends Model
{

    use HasFactory;

    protected $table = 'payments';
    protected $guarded = [];

    /**
     * Get the Payment user
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function plan()
    {
        return $this->belongsTo(Plans::class, 'plan_id');
    }
}
