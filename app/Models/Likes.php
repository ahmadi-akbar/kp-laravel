<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{
    public $timestamps = false;
    protected $table = 'likes';

    protected $fillable = [
        'music_id',
        'user_id',
    ];

    public function music()
    {
        return $this->belongsTo(Musics::class);
    }
}
