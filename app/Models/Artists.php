<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;

class Artists extends Model
{

    use HasFactory;

    protected $table = 'artists';

    public function music()
    {
        return $this->hasMany(Musics::class, "artist_id");
    }
    public function follow()
    {
        return $this->hasOne(Follows::class, 'artist_id');
    }

    public function isFollowed()
    {
        return $this->follow()->where('user_id',  Auth::id())->exists();
    }
}
