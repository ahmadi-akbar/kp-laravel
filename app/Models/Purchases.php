<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    protected $table = 'purchases';

    protected $fillable = [
        'music_id',
        'user_id',
    ];

    public function music()
    {
        return $this->belongsTo(Musics::class, 'music_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
