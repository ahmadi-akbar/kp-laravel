<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follows extends Model
{
    public $timestamps = false;
    protected $table = 'follows';

    protected $fillable = [
        'artist_id',
        'user_id',
    ];

    public function artist()
    {
        return $this->belongsTo(Artists::class);
    }
}
