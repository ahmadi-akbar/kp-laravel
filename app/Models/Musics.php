<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;

class Musics extends Model
{

    use HasFactory;

    protected $table = 'musics';
    protected $guarded = [];

    /**
     * Get the Artist that owns the Music.
     */
    public function artist()
    {
        return $this->belongsTo(Artists::class, 'artist_id');
    }

    public function like()
    {
        return $this->hasOne(Likes::class, 'music_id');
    }
    public function isLiked()
    {
        return $this->like()->where('user_id',  Auth::id())->exists();
    }

    public function purchase()
    {
        return $this->hasOne(Purchases::class, 'music_id');
    }
    public function isPurchased()
    {
        return $this->purchase()->where('user_id',  Auth::id())->exists();
    }
}
