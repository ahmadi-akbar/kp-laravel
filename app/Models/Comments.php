<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';

    protected $fillable = [
        'music_id',
        'user_id',
        "text"
    ];

    public function music()
    {
        return $this->belongsTo(Musics::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
