<?php

use Illuminate\Support\Facades\Route;


// Auth Routes
Route::post('/auth/register', 'AuthController@register');
Route::post('/auth/login', 'AuthController@login');
Route::post('/auth/forget-pass', 'AuthController@forgetPass');


Route::middleware(["auth:sanctum"])->group(function () {
    Route::get('/user', 'UserController@index');
    Route::post('/user', 'UserController@update');
    Route::get('/user/follows', 'UserController@getFollowList');
    Route::get('/user/likes', 'UserController@getLikeList');
    Route::post('/user/fcm_token', 'UserController@saveFcmToken');
    // get the purchased Musics by User
    Route::get('/user/musics', 'UserController@getPurchasedMusicList');
    // get the requested plan list by User
    Route::get('/user/payments', 'UserController@getPayments');

    // Artist
    Route::get('/artist', 'ArtistsController@index');
    Route::get('/artist/{id}', 'ArtistsController@getOne');
    Route::get('/artist/{id}/music', 'ArtistsController@getMusics');
    Route::post('/artist/{id}/follow', 'ArtistsController@follow');
    Route::post('/artist/{id}/unfollow', 'ArtistsController@unFollow');

    // Music
    Route::get('/music', 'MusicsController@index');
    Route::get('/music/{id}', 'MusicsController@getOne');
    // Music comment
    Route::get('/music/{id}/comment', 'MusicsController@getComments');
    Route::post('/music/{id}/comment', 'MusicsController@addComment');
    Route::post('/music/{id}/purchase', 'MusicsController@purchaseMusic');

    Route::post('/music/{id}/like', 'MusicsController@like');
    Route::post('/music/{id}/unlike', 'MusicsController@unLike');

    // Plans
    Route::get('/plans', 'PlansController@index');
    Route::post('/plans/{id}', 'PlansController@purchase');


    // Search
    Route::get('/search', 'MusicsController@search');


    //
    Route::middleware(['throttle:20,1'])->group(function () {
        Route::post('/user/change-pass', 'AuthController@changePass');
    });
});
