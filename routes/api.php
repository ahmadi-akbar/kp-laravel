<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



/*
|--------------------------------------------------------------------------
| Api v1
|--------------------------------------------------------------------------
|
*/

Route::group([
    'namespace'  => 'Api',
    'prefix'     => 'v1',
], function ($router) {
    require base_path('routes/api.v1.php');
});
