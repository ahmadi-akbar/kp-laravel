<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['get.menu']], function () {
    Auth::routes();
    Route::group(['middleware' => ['role:admin']], function () {
        Route::get('/', 'UsersController@index');
        Route::resource('users', 'UsersController')->except(['create', 'store']);
        Route::get('/user/configs', 'UsersController@configView');
        Route::post('/user/configs', 'UsersController@configStore');
        Route::resource('artists', 'ArtistsController');
        Route::resource('musics', 'MusicsController');
        Route::resource('plans', 'PlansController');
        Route::resource('payments', 'PaymentsController');
        Route::resource('comments', 'CommentsController');
        Route::post('upload', 'UploadController@index')->name('upload');


        Route::get('/clear-cache', function () {
            return Artisan::call('optimize:clear');
        });
    });
});
